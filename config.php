<?php

/*
|--------------------------------------------------------------------------
| Default Theme
|--------------------------------------------------------------------------
|
| This is where you can set the default theme of the website.  To view the
| themes that are included, please visit: http://bootswatch.com/
|
| Valid themes are:
| default, amelia, cerulean, cosmo, cyborg, flatly, journal, lumen, 
| readable, simplex, slate, spacelab, superhero, united, yeti
|
*/
$config['site.defaulttheme'] = 'flatly';

/*
|--------------------------------------------------------------------------
| Website Name
|--------------------------------------------------------------------------
|
| This is where you can name your site, for example FireBall's Mods
|
*/
$config['site.sitename'] = "My Forge Mods";

/*
|--------------------------------------------------------------------------
| Links
|--------------------------------------------------------------------------
|
| This is where you can set where the links on the site go too.  Any links
| that are left blank will not be shown on the site...
|
| Home: Your website's home page
| GitHub: Github URL
| BitBucket: BitBucket URL
| Twitter: Twitter URL
| Facebook: Facebook URL
| Donate: Donation URL
|
| Make sure to include the http:// or https:// or url's will not link.
|
*/
$config['site.link.home'] = "http://www.tsr.me";
$config['site.link.github'] = "http://www.github.com/FireBall1725";
$config['site.link.bitbucket'] = "http://www.bitbucket.com/FireBall1725";
$config['site.link.twitter'] = "http://www.twitter.com/fireball1725";
$config['site.link.facebook'] = "";
$config['site.link.donate'] = "";

/*
|--------------------------------------------------------------------------
| Google Analytics, Google Ads, AdFly Links 
|--------------------------------------------------------------------------
|
| This is where you can set your Google Analytics, Ads or enable AdFly
| links.
|
| Google Analytics: Requires site id, starts with UA
| Google Ads: Not yet supported, soon
| AdFly: Not yet supported, soon
*/
$config['site.analytics.siteid'] = "";

/*
|--------------------------------------------------------------------------
| Directory Configuration
|--------------------------------------------------------------------------
|
| This is where all your mods are going to be stored on the system.  this
| folder will need to be in your public_html or web accessable in order for
| the download links to work correctly.  If you do not want to use the
| default folder name you can change it here.
|
*/
$config['site.modsfolder'] = "mods";

/*
|--------------------------------------------------------------------------
| Site Copyright
|--------------------------------------------------------------------------
|
| This is where you can configure the copyright shown at the bottom of the
| website.  You can also choose to disable the powered by message if you
| wish, however I would appreciate if you removed the powered by line that
| you give me credit somewhere for the site.
|
*/
$config['site.copyright'] = "Copyright 2014";
$config['site.showpoweredby'] = true; 
?>
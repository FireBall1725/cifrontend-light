<?php /* Smarty version Smarty-3.1.17, created on 2014-06-10 19:08:06
         compiled from "application/views/mainpage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:100907285753950efc8296a0-08781303%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3802715fcc8265c1c43b87bfd8caaa6cab3dd65' => 
    array (
      0 => 'application/views/mainpage.tpl',
      1 => 1402441686,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '100907285753950efc8296a0-08781303',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_53950efc84ad96_39128989',
  'variables' => 
  array (
    'modversions' => 0,
    'modname' => 0,
    'verdata' => 0,
    'selversion' => 0,
    'k' => 0,
    'selmod' => 0,
    'modreadme' => 0,
    'changelog' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53950efc84ad96_39128989')) {function content_53950efc84ad96_39128989($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('template_siteheader.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div class="row">
	<?php if ($_smarty_tpl->tpl_vars['modversions']->value!=null) {?>
	<div class="col-md-4">

		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $_smarty_tpl->tpl_vars['modname']->value;?>
</h3>
			</div>
			<div class="panel-body">
				
				
				<?php if ($_smarty_tpl->tpl_vars['verdata']->value["thumbnailimage.filename"]!='') {?><img src="<?php echo $_smarty_tpl->tpl_vars['verdata']->value["thumbnailimage.filename"];?>
" class="img-responsive img-thumbnail"><?php }?>

				<h3>Minecraft Version</h3>
				
				<ul class="nav nav-pills nav-stacked">
				<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['modversions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value) {
$_smarty_tpl->tpl_vars['i']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['i']->key;
?>
					<li <?php if ($_smarty_tpl->tpl_vars['selversion']->value==$_smarty_tpl->tpl_vars['k']->value) {?>class="active"<?php }?>><a href="?q=<?php echo $_smarty_tpl->tpl_vars['selmod']->value;?>
&v=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><span class="glyphicon glyphicon-chevron-right"></span> <?php echo $_smarty_tpl->tpl_vars['k']->value;?>
</a></li>
				<?php } ?>
				</ul>

				<h3>Navigation</h3>
				<ul class="nav nav-pills nav-stacked">
					<?php if ($_smarty_tpl->tpl_vars['verdata']->value["sourcecode.readmeurl"]!='') {?><li class="active"><a href="#modinfo" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Mod Info</a></li><?php }?>
					<li <?php if ($_smarty_tpl->tpl_vars['verdata']->value["sourcecode.readmeurl"]=='') {?>class="active"<?php }?>><a href="#moddownloads" data-toggle="tab"><span class="glyphicon glyphicon-cloud-download"></span> All Downloads</a></li>
					<?php if ($_smarty_tpl->tpl_vars['verdata']->value["changelog.filename"]!='') {?><li><a href="#changelog" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Changelog</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['verdata']->value["sourcecode.url"]!='') {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['verdata']->value["sourcecode.url"];?>
"><span class="glyphicon glyphicon-link"></span> Source Code</a></li><?php }?>
					<li class="nav-divider"></li>
					<li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span> Latest Download</a><small>**filename.something.jar**<br>Dec 12, 2013</small></li>
					<!--<li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span> Recommended Download</a><small>**filename.something.jar**<br>Dec 8, 2013</small></li>-->
				</ul>
			</div>
		</div>

	</div>

	<div class="col-md-8">

		<div class="tab-content">

			<?php if ($_smarty_tpl->tpl_vars['verdata']->value["sourcecode.readmeurl"]!='') {?>
			<div class="tab-pane active" id="modinfo">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Mod Info</h3>
					</div>
					<div class="panel-body">
						<div id="modreadme">
							<?php echo $_smarty_tpl->tpl_vars['modreadme']->value;?>

						</div>
					</div>
				</div>
			</div>
			<?php }?>

			<div class="tab-pane <?php if ($_smarty_tpl->tpl_vars['verdata']->value["sourcecode.readmeurl"]=='') {?>active<?php }?>" id="moddownloads">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">All Downloads</h3>
					</div>
					<div class="panel-body">
						<table id="downloads" class="table table-hover">
							<thead>
								<tr>
									<th>Version</th>
									<th>Last Modified</th>
									<?php if ($_smarty_tpl->tpl_vars['verdata']->value["changelog.eachfile"]==true) {?><th>Changelog</th><?php }?>
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<?php if ($_smarty_tpl->tpl_vars['verdata']->value["changelog.filename"]!='') {?>
			<div class="tab-pane" id="changelog">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Changelog</h3>
					</div>
				<div class="panel-body">
					<p><?php echo $_smarty_tpl->tpl_vars['changelog']->value;?>
</p>
				</div>
			</div>
			<?php }?>
			
		</div>
	</div>
	<?php } else { ?>
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Error with <?php echo $_smarty_tpl->tpl_vars['modname']->value;?>
</h3>
			</div>

			<div class="panel-body">
				<p>This mod is not yet setup, please check back later...</p>
			</div>
		</div>
	</div>
	<?php }?>

</div>

<?php echo $_smarty_tpl->getSubTemplate ('template_sitefooter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>

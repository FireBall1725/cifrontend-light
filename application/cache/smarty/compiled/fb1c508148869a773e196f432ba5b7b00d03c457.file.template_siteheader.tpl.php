<?php /* Smarty version Smarty-3.1.17, created on 2014-06-10 17:13:32
         compiled from "application/views/template_siteheader.tpl" */ ?>
<?php /*%%SmartyHeaderCode:196195894653950f14a41da4-43639729%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fb1c508148869a773e196f432ba5b7b00d03c457' => 
    array (
      0 => 'application/views/template_siteheader.tpl',
      1 => 1402434810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '196195894653950f14a41da4-43639729',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_53950f14a60d31_60936254',
  'variables' => 
  array (
    'this' => 0,
    'mods' => 0,
    'k' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53950f14a60d31_60936254')) {function content_53950f14a60d31_60936254($_smarty_tpl) {?><!DOCTYPE html><?php $_smarty_tpl->tpl_vars["theme"] = new Smarty_variable($_smarty_tpl->tpl_vars['this']->value->config->item('site.defaulttheme'), null, 0);?>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $_smarty_tpl->tpl_vars['this']->value->config->item('site.sitename');?>
</title>

	<!-- Bootstrap -->
	<?php echo css(((string)$_smarty_tpl->tpl_vars['theme']->value)."/bootstrap.min.css");?>

	<?php echo css('custom.css');?>

	
	
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
  
<body>
	<div class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="#"><?php echo $_smarty_tpl->tpl_vars['this']->value->config->item('site.sitename');?>
</a>
			</div>

			<div class="navbar-collapse collapse navbar-responsive-collapse">

				<ul class="nav navbar-nav">
					<?php if ($_smarty_tpl->tpl_vars['this']->value->config->item('site.link.home')!='') {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['this']->value->config->item('site.link.home');?>
">Home</a></li><?php }?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Mods <b class="caret"></b></a>
						<ul class="dropdown-menu">
						<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['mods']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
							<li><a href="?q=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['modname'];?>
</a></li>
						<?php } ?>
						</ul>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<?php if ($_smarty_tpl->tpl_vars['this']->value->config->item('site.link.github')!='') {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['this']->value->config->item('site.link.github');?>
" target="_new">Github</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['this']->value->config->item('site.link.bitbucket')!='') {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['this']->value->config->item('site.link.bitbucket');?>
" target="_new">BitBucket</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['this']->value->config->item('site.link.twitter')!='') {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['this']->value->config->item('site.link.twitter');?>
" target="_new">Twitter</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['this']->value->config->item('site.link.facebook')!='') {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['this']->value->config->item('site.link.facebook');?>
" target="_new">Facebook</a></li><?php }?>
					<?php if ($_smarty_tpl->tpl_vars['this']->value->config->item('site.link.donate')!='') {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['this']->value->config->item('site.link.donate');?>
" target="_new">Donate</a></li><?php }?>
				</ul>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="container"><?php }} ?>

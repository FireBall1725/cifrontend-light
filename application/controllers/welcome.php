<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// Get all mods
		$mods = $this->modfolderenumerator->get_modfolder();
		$data['mods'] = $mods;
		$modids = array_keys($mods);
		
		$modid=(array_shift(array_values($modids)));		
		if(isset($_GET['q'])) {
			$modid = $_GET['q'];
		}
		$data['selmod'] = $modid;
		
		$data['modname'] = $mods[$modid]['modname'];
		
		// Get versions
		$modversions = null;
		$selversion = "";
		$verdata = null;
		if (isset($mods[$modid]['versions'])) {
			$modversions = $mods[$modid]['versions'];
			arsort($modversions);
			
			$selversion = key($modversions);
			
			if (isset($_GET['v'])) {
				$selversion = $_GET['v'];
			}

			$verdata = $mods[$modid]['versions'][$selversion];		
		}
		
		//var_dump($verdata);
				
		$data['modversions'] = $modversions;
		$data['selversion'] = $selversion;
		$data['verdata'] = $verdata;
		//var_dump($modversion);
		
		$this->parser->parse('mainpage.tpl', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
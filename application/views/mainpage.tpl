{include file='template_siteheader.tpl'}

<div class="row">
	{if $modversions ne null}
	<div class="col-md-4">

		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">{$modname}</h3>
			</div>
			<div class="panel-body">
				
				
				{if $verdata["thumbnailimage.filename"] ne ''}<img src="{$verdata["thumbnailimage.filename"]}" class="img-responsive img-thumbnail">{/if}

				<h3>Minecraft Version</h3>
				
				<ul class="nav nav-pills nav-stacked">
				{foreach from=$modversions key=k item=i}
					<li {if $selversion eq $k}class="active"{/if}><a href="?q={$selmod}&v={$k}"><span class="glyphicon glyphicon-chevron-right"></span> {$k}</a></li>
				{/foreach}
				</ul>

				<h3>Navigation</h3>
				<ul class="nav nav-pills nav-stacked">
					{if $verdata["sourcecode.readmeurl"] ne ''}<li class="active"><a href="#modinfo" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Mod Info</a></li>{/if}
					<li {if $verdata["sourcecode.readmeurl"] eq ''}class="active"{/if}><a href="#moddownloads" data-toggle="tab"><span class="glyphicon glyphicon-cloud-download"></span> All Downloads</a></li>
					{if $verdata["changelog.filename"] ne ''}<li><a href="#changelog" data-toggle="tab"><span class="glyphicon glyphicon-list-alt"></span> Changelog</a></li>{/if}
					{if $verdata["sourcecode.url"] ne ''}<li><a href="{$verdata["sourcecode.url"]}"><span class="glyphicon glyphicon-link"></span> Source Code</a></li>{/if}
					<li class="nav-divider"></li>
					<li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span> Latest Download</a><small>**filename.something.jar**<br>Dec 12, 2013</small></li>
					<!--<li><a href="#"><span class="glyphicon glyphicon-cloud-download"></span> Recommended Download</a><small>**filename.something.jar**<br>Dec 8, 2013</small></li>-->
				</ul>
			</div>
		</div>

	</div>

	<div class="col-md-8">

		<div class="tab-content">

			{if $verdata["sourcecode.readmeurl"] ne ''}
			<div class="tab-pane active" id="modinfo">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Mod Info</h3>
					</div>
					<div class="panel-body">
						<div id="modreadme">
							{$modreadme}
						</div>
					</div>
				</div>
			</div>
			{/if}

			<div class="tab-pane {if $verdata["sourcecode.readmeurl"] eq ''}active{/if}" id="moddownloads">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">All Downloads</h3>
					</div>
					<div class="panel-body">
						<table id="downloads" class="table table-hover">
							<thead>
								<tr>
									<th>Version</th>
									<th>Last Modified</th>
									{if $verdata["changelog.eachfile"] eq true}<th>Changelog</th>{/if}
								</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			{if $verdata["changelog.filename"] ne ''}
			<div class="tab-pane" id="changelog">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Changelog</h3>
					</div>
				<div class="panel-body">
					<p>{$changelog}</p>
				</div>
			</div>
			{/if}
			
		</div>
	</div>
	{else}
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Error with {$modname}</h3>
			</div>

			<div class="panel-body">
				<p>This mod is not yet setup, please check back later...</p>
			</div>
		</div>
	</div>
	{/if}

</div>

{include file='template_sitefooter.tpl'}
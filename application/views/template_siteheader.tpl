<!DOCTYPE html>{assign var="theme" value=$this->config->item('site.defaulttheme')}
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{$this->config->item('site.sitename')}</title>

	<!-- Bootstrap -->
	{css("$theme/bootstrap.min.css")}
	{css('custom.css')}
	
	
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
  
<body>
	<div class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="#">{$this->config->item('site.sitename')}</a>
			</div>

			<div class="navbar-collapse collapse navbar-responsive-collapse">

				<ul class="nav navbar-nav">
					{if $this->config->item('site.link.home') ne ''}<li><a href="{$this->config->item('site.link.home')}">Home</a></li>{/if}
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Mods <b class="caret"></b></a>
						<ul class="dropdown-menu">
						{foreach from=$mods key=k item=v}
							<li><a href="?q={$k}">{$v['modname']}</a></li>
						{/foreach}
						</ul>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					{if $this->config->item('site.link.github') ne ''}<li><a href="{$this->config->item('site.link.github')}" target="_new">Github</a></li>{/if}
					{if $this->config->item('site.link.bitbucket') ne ''}<li><a href="{$this->config->item('site.link.bitbucket')}" target="_new">BitBucket</a></li>{/if}
					{if $this->config->item('site.link.twitter') ne ''}<li><a href="{$this->config->item('site.link.twitter')}" target="_new">Twitter</a></li>{/if}
					{if $this->config->item('site.link.facebook') ne ''}<li><a href="{$this->config->item('site.link.facebook')}" target="_new">Facebook</a></li>{/if}
					{if $this->config->item('site.link.donate') ne ''}<li><a href="{$this->config->item('site.link.donate')}" target="_new">Donate</a></li>{/if}
				</ul>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="container">
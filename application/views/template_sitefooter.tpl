	</div>
	<div id="footer">
		<div class="container">
			<p class="text-muted">{$this->config->item('site.copyright')}</p>
			{if $this->config->item('site.showpoweredby') eq true}<p class="text-muted">Powered by <a href="https://bitbucket.org/FireBall1725/cifrontend-light">CI FrontEnd (Light)</a> Created By: <a href="https://twitter.com/fireball1725">FireBall1725</a>, Website themes by: <a href="http://bootswatch.com/">http://bootswatch.com/</a></p>{/if}
		</div>
	</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {js('bootstrap.min.js')}
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf8" src="http://cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	
	{literal}
	<script type="text/javascript">
		$(document).ready( function () {
			$('#downloads').DataTable( {
				"bFilter": false,
				"bSort": false,
				"bLengthChange": false
			} );
		} );
	</script>
	{/literal}
	
</body>
</html>
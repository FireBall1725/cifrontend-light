<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * Mod Folder Enumerator
 *
 * Folder Enumerator for Codeigniter
 *
 * @package   
 * @author      FireBall1725
 * @copyright   2014 FireBall1725
 * @link        http://www.tsr.me
 * @license     
 * @version     1.0
 */

class ModFolderEnumerator {
    protected $CI;

    public function __construct()
    {
        // Codeigniter instance and other required libraries/files
        $this->CI = get_instance();
        
     	//var_dump($this->get_modfolder()); 
    }
    
    public function get_modfolder()
    {
    	$directory = $this->CI->config->item('site.modsfolder');
    	
        $moddir = $this->dir_list($directory);

		// Get Mods
		foreach($moddir as $k => $v) {
			if ($v['type'] == 'dir') {
				$foldername = $v['name'];
				if (file_exists("$directory/$foldername/modinfo.json")) {
					$modinfo = json_decode(file_get_contents("$directory/$foldername/modinfo.json"), true);
					$modinfo['dirname'] = $foldername;
					$mods[] = $modinfo;
				}
			}
		}
		
		asort($mods);
		
		// Get Mod Version
		foreach($mods as $modkey => $modinfo) {
			$modfolder = $modinfo['dirname'];
			$moddir = $this->dir_list("$directory/$modfolder");
			
			foreach($moddir as $k => $v) {
				if ($v['type'] == 'dir') {
					$foldername = $v['name'];
					if (file_exists("$directory/$modfolder/$foldername/versioninfo.json")) {
						$versioninfo = json_decode(file_get_contents("$directory/$modfolder/$foldername/versioninfo.json"), true);
						//$v[$v['name'] = $versioninfo;
						$mods[$modkey]['versions'][$v['name']] = $versioninfo;
					}
				}
			}
		}

		return $mods;
    }
    
    protected function dir_list($dir) {
		if ($dir[strlen($dir)-1] != '/') $dir .= '/';
		
		if (!is_dir($dir)) return array();
		
		$dir_handle	 = opendir($dir);
		$dir_objects = array();
		while ($object = readdir($dir_handle))
		if (!in_array($object, array('.','..'))) {
			$filename	 = $dir . $object;
			$file_object = array(
				'name' => $object,
				'size' => filesize($filename),
				'type' => filetype($filename),
				'time' => date("d F Y H:i:s", filemtime($filename))
			);
			$dir_objects[] = $file_object;
		}	
	
		return $dir_objects;
	}
}
?>